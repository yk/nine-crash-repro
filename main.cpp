#include <windows.h>
#include <d3d9.h>

#define D3DFVF_VOLUMEVERTEX (D3DFVF_XYZ|D3DFVF_DIFFUSE|D3DFVF_TEX1|D3DFVF_TEXCOORDSIZE3(0))

int WINAPI
WinMain(HINSTANCE hInstance,
        HINSTANCE hPrevInstance,
        LPSTR lpCmdLine,
        int nCmdShow)
{
   HRESULT hr;
   WNDCLASSEX wc =
   {
      sizeof(WNDCLASSEX),
      CS_CLASSDC,
      DefWindowProc,
      0,
      0,
      hInstance,
      NULL,
      NULL,
      NULL,
      NULL,
      "a",
      NULL
    };
   RegisterClassEx(&wc);

   const int WindowWidth = 512;
   const int WindowHeight = 512;

   DWORD dwStyle = WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_OVERLAPPEDWINDOW;

   RECT rect = {0, 0, WindowWidth, WindowHeight};
   AdjustWindowRect(&rect, dwStyle, FALSE);

   HWND hWnd = CreateWindow(wc.lpszClassName,
                            "Gallium Nine crash reproducer",
                            dwStyle,
                            CW_USEDEFAULT, CW_USEDEFAULT,
                            rect.right - rect.left,
                            rect.bottom - rect.top,
                            NULL,
                            NULL,
                            hInstance,
                            NULL);
   if (!hWnd) {
      return 1;
   }

   ShowWindow(hWnd, nCmdShow);

   LPDIRECT3D9 pD3D = NULL;
   pD3D = Direct3DCreate9(D3D_SDK_VERSION);
   if (!pD3D) {
      return 1;
   }

   D3DCAPS9 caps;
   hr = pD3D->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &caps);
   if (FAILED(hr)) {
      return 1;
   }

   DWORD dwBehaviorFlags;
   if ((caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT) == 0 ||
       caps.VertexShaderVersion < D3DVS_VERSION(1, 1)) {
      dwBehaviorFlags = D3DCREATE_SOFTWARE_VERTEXPROCESSING;
   } else {
      dwBehaviorFlags = D3DCREATE_HARDWARE_VERTEXPROCESSING;
   }

   D3DPRESENT_PARAMETERS PresentationParameters;
   ZeroMemory(&PresentationParameters, sizeof PresentationParameters);
   PresentationParameters.Windowed = TRUE;
   PresentationParameters.BackBufferCount = 1;
   PresentationParameters.SwapEffect = D3DSWAPEFFECT_DISCARD;
   PresentationParameters.BackBufferFormat = D3DFMT_X8R8G8B8;
   PresentationParameters.hDeviceWindow = hWnd;

   PresentationParameters.EnableAutoDepthStencil = FALSE;
   PresentationParameters.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;

   IDirect3DDevice9 *pDevice = NULL;
   hr = pD3D->CreateDevice(D3DADAPTER_DEFAULT,
                           D3DDEVTYPE_HAL,
                           hWnd,
                           dwBehaviorFlags,
                           &PresentationParameters,
                           &pDevice);
   if (FAILED(hr)) {
      return 1;
   }

   struct VOLUMEVERTEX
   {
      FLOAT x, y, z;
      DWORD color;
      FLOAT tu, tv, tw;
   };

   VOLUMEVERTEX vertices[4] =
   {
      { 1.0f, 1.0f, 0.0f, 0xffffffff, 1.0f, 1.0f, 0.0f },
      {-1.0f, 1.0f, 0.0f, 0xffffffff, 0.0f, 1.0f, 0.0f },
      { 1.0f,-1.0f, 0.0f, 0xffffffff, 1.0f, 0.0f, 0.0f },
      {-1.0f,-1.0f, 0.0f, 0xffffffff, 0.0f, 0.0f, 0.0f }
   };

   IDirect3DVertexBuffer9 *vbuffer = NULL;

   pDevice->CreateVertexBuffer(4 * sizeof(VOLUMEVERTEX),
                               0,
                               D3DFVF_VOLUMEVERTEX,
                               D3DPOOL_MANAGED,
                               &vbuffer,
                               NULL);

   IDirect3DVolumeTexture9 *tex;
   pDevice->CreateVolumeTexture(32, 32, 32,
                                0,
                                0,
                                D3DFMT_A8R8G8B8,D3DPOOL_MANAGED,
                                &tex,
                                NULL);

   MSG msg;

   while(TRUE)
   {
      while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
      {
         TranslateMessage(&msg);
         DispatchMessage(&msg);
      }

      if(msg.message == WM_QUIT)
         break;

      pDevice->BeginScene();
      pDevice->SetTexture(0, tex);
      pDevice->SetFVF(D3DFVF_VOLUMEVERTEX);

      VOID* pVoid;
      vbuffer->Lock(0, 0, (void**)&pVoid, 0);
      memcpy(pVoid, vertices, sizeof(vertices));
      vbuffer->Unlock();

      pDevice->SetStreamSource(0, vbuffer, 0, sizeof(VOLUMEVERTEX));
      for (int i = 0; i < 100; i++)
         pDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
      pDevice->EndScene();
      pDevice->Present(NULL, NULL, NULL, NULL);
   }

   DestroyWindow(hWnd);

   return 0;
}

LRESULT CALLBACK
WindowProc(HWND hWnd,
           UINT message,
           WPARAM wParam,
           LPARAM lParam)
{
   switch(message) {
   case WM_DESTROY:
   {
      PostQuitMessage(0);
      return 0;
   } break;
   }

   return DefWindowProc (hWnd, message, wParam, lParam);
}
